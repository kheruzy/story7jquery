import datetime
import os

from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
from .views import *

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exists(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_uses_view(self):
        handler = resolve('/')
        self.assertEqual(handler.func, story7app)

    def test_uses_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_buton_exists(self):
        response = self.client.get('')
        html = response.content.decode("utf8")
        self.assertIn("buttonID",html)

# class FunctionalTest(LiveServerTestCase):

#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.browser = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
#         super(FunctionalTest,self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(FunctionalTest, self).tearDown()

#     def theme_change_test(self):
#         selenium = self.browser
#         selenium.get(self.live_server_url + '/')
#         font_color = selenium.find_element_by_id('hicolor').value_of_css_property(color)
#         self.assertEqual(font_color,'#ffffff')
#         button = selenium.find_element_by_id('buttonID')
#         time.sleep(1)
#         button.click()
#         font_color = selenium.find_element_by_id('hicolor').value_of_css_property(color)
#         self.assertEqual(font_color,'#000000')
#         time.sleep(1)