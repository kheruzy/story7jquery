from django.urls import path
from . import views

app_name = 'story7app'

urlpatterns = [
    path('', views.story7app, name='story7app'),
]