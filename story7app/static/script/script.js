(function($) {
    $('.accordion > li:eq(0) a').addClass('active').next().slideDown();

    $('.accordion a').click(function(j) {
        var dropDown = $(this).closest('li').find('p');

        $(this).closest('.accordion').find('p').not(dropDown).slideUp();

        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).closest('.accordion').find('a.active').removeClass('active');
            $(this).addClass('active');
        }

        dropDown.stop(false, true).slideToggle();

        j.preventDefault();
    });
})(jQuery);

var count = 0
window.onload = function(){
    document.getElementById('buttonID').onclick =  function () {
        count = count + 1  ;
        if (count%2 == 0){
        console.log(count);
        document.getElementById('theme').href = '/static/css/dark.css';
        } else {
        console.log(count);
        document.getElementById('theme').href = '/static/css/light.css';
        }
    };
}

$(document).ready(function(){
    search();
    // Execute a function when the user releases a key on the keyboard
    input.addEventListener("keyup", function(event) {
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13) {
            // Cancel the default action, if needed
            event.preventDefault();
            // Trigger the button element with a click
            document.getElementById("submit").click();
        }
    });
    
    $('#submit').click(function(event){
        query = document.getElementById("searchbox").value;
        search();
    })

    
})

var query = 'kontol';

var input = document.getElementById("searchbox");

function search(){
    var link = 'https://www.googleapis.com/books/v1/volumes?q=';
    console.log(query);
    var words = query.split(' ');
    for (i = 0; i < words.length; i++) {
        link += words[i];
        if (i < words.length-1) {
            link += '+';
        }
    }

    var table = '<tr><th id = "number">No.</th><th id = "title">Title</th><th id = "author">Author</th><th id = "publisher">Publisher</th><th id = "year">Year</th></tr>';
    var noinfo = '<td style = "color: #bbb">No info</td>';
    var counter = 1;

    console.log(link);

    $.ajax({
        url: link,
        success: function(data){
            $('#book_container').empty();
    
            $.each(data.items, function(i, item){
                table += '<tr>';
                table += '<td>' + counter + '</td>';
                table += '<td><a class = "link" href = "' + item.volumeInfo.infoLink + '"  target = "_blank">' + item.volumeInfo.title + '</a></td>';
    
                if (!item.volumeInfo.authors) {
                    table += noinfo;
                }
                else {
                    table += '<td>'
                    var authors = ''
                    authors += item.volumeInfo.authors;
                    var author = authors.split(',');
                    for (i = 0; i < author.length; i++) {
                        table += author[i];
                        if (i < author.length - 1) {
                            table += ', '
                        }
                    }
                    table += '</td>';
                }
    
                if (!item.volumeInfo.publisher) {
                    table += noinfo;
                }
                else {
                    table += '<td>' + item.volumeInfo.publisher + '</td>';
                }
    
                if (!item.volumeInfo.publishedDate) {
                    table += noinfo;
                }
                else {
                    var date = item.volumeInfo.publishedDate;
                    var year = date.substring(0,4);
                    table += '<td>' + year + '</td>';
                }
                table += '</tr>';
                counter++;
            })
            
            $('#book_container').append(table);
        }})}
