from django.shortcuts import render
from . import forms

# Create your views here.
def story8app(request):
    return render(request, 'story8/index.html')