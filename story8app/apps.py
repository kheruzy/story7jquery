from django.apps import AppConfig


class Story8AppConfig(AppConfig):
    name = 'story8app'
