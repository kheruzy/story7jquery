from django.urls import path
from . import views

app_name = 'story8app'

urlpatterns = [
    path('', views.story8app, name='story8app'),
]