import datetime
import os

from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
from .views import *

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exists(self):
        response = self.client.get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_uses_view(self):
        handler = resolve('/story8/')
        self.assertEqual(handler.func, story8app)

    def test_uses_template(self):
        response = self.client.get('/story8/')
        self.assertTemplateUsed(response, 'story8/index.html')

    def test_buton_exists(self):
        response = self.client.get('/story8/')
        html = response.content.decode("utf8")
        self.assertIn("submit",html)