from django.urls import path
from . import views

app_name = 'story9app'

urlpatterns = [
    path('', views.story9app, name='story9app'),
    path('register', views.register, name='register'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
]