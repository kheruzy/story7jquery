from django.shortcuts import render, redirect
from django.contrib.auth.models import User, auth
from django.contrib import messages

# Create your views here.

def story9app(request):
    return render(request, 'story9/index.html')

def register(request):
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password1 = request.POST['password1']
        password2 = request.POST['password2']

        if username and email and password1 and password2:
            if "@" in username:
                messages.info(request, "Username can't contain @ in it")
                return redirect('/story9/register')

            if User.objects.filter(username=username).exists():
                messages.info(request, "Username already taken")
                if User.objects.filter(email=email).exists():
                    messages.info(request, 'Email already used')
                    if password1 != password2:
                        messages.info(request, "Password didn't match.")
                        return redirect('/story9/register')
                    else:
                        messages.info(request, "Please log in")
                        return redirect('/story9/login')
                else:
                    messages.info(request, "Please log in")
                    return redirect('/story9/login')
                    
            else:
                # print("Password did't match") 
                user = User.objects.create_user(username=username, email=email, password=password1)
                user.save()
                # print('User Created')
                user = auth.authenticate(username=username, password=password1)
                # messages.info(request, 'User Created')
                auth.login(request, user)
                return redirect('/story9/')
        else:
            messages.info(request, "Please fill the form first.")
            return redirect('/story9/register')

    else:
        return render(request, 'story9/register.html')

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        if("@" in username):
            user = auth.authenticate(email=username, password=password)
        else:
            user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            return redirect('/story9/')
        else:
            messages.info(request, 'Invalid information')
            return redirect('/story9/login')

    else:
        return render(request, 'story9/login.html')

def logout(request):
    auth.logout(request)
    request.session.flush()
    return redirect('/story9/')
