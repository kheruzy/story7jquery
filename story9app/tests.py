import datetime
import os

from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.contrib.auth.models import User, auth
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
from .models import *

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exists(self):
        response = self.client.get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_uses_view(self):
        handler = resolve('/story9/')
        self.assertEqual(handler.func, story9app)
        handler = resolve('/story9/register')
        self.assertEqual(handler.func, register)
        handler = resolve('/story9/login')
        self.assertEqual(handler.func, login)

    def test_uses_template(self):
        response = self.client.get('/story9/')
        self.assertTemplateUsed(response, 'story9/index.html')
        response = self.client.get('/story9/register')
        self.assertTemplateUsed(response, 'story9/register.html')
        response = self.client.get('/story9/login')
        self.assertTemplateUsed(response, 'story9/login.html')

    def test_user_save(self):
        handler = resolve('/story9/register')
        username = 'user'
        email = 'user@a.com'
        password = 'password'
        user = User.objects.create_user(username=username, email=email, password=password)
        user.save()
        user = User.objects.all()[0]
        self.assertEqual(user.username, 'user')
        self.assertEqual(user.email, 'user@a.com')


class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_story_pass_didnt_match(self):
        selenium = self.browser
        selenium.get(self.live_server_url + '/story9/register')

        username = selenium.find_element_by_name('username')
        email = selenium.find_element_by_name('email')
        password1 = selenium.find_element_by_name('password1')
        password2 = selenium.find_element_by_name('password2')
        username.send_keys('Lorem')
        # time.sleep(1)
        email.send_keys('Ipsum')
        # time.sleep(1)
        password1.send_keys('Sit')
        # time.sleep(1)
        password2.send_keys('Dolor')
        # time.sleep(1)
        submit = selenium.find_element_by_id('buttonID')
        # time.sleep(1)
        submit.click()
        # time.sleep(5)
        self.assertIn("I already have an account", selenium.page_source)

    def test_story_pass_match(self):
        selenium = self.browser
        selenium.get(self.live_server_url + '/story9/register')

        username = selenium.find_element_by_id("username")
        email = selenium.find_element_by_id('email')
        password1 = selenium.find_element_by_id('password1')
        password2 = selenium.find_element_by_id('password2')
        username.send_keys('Lorem')
        # time.sleep(1)
        email.send_keys('Ipsum')
        # time.sleep(1)
        password1.send_keys('Sit')
        # time.sleep(1)
        password2.send_keys('Sit')
        # time.sleep(1)
        submit = selenium.find_element_by_id('buttonID')
        # time.sleep(1)
        submit.click()
        # time.sleep(1)
        self.assertNotIn("STRANGER", selenium.page_source)

    def test_story_signup_login(self):
        selenium = self.browser
        selenium.get(self.live_server_url + '/story9/register')

        username = selenium.find_element_by_id("username")
        email = selenium.find_element_by_id('email')
        password1 = selenium.find_element_by_id('password1')
        password2 = selenium.find_element_by_id('password2')
        username.send_keys('Lorem')
        # time.sleep(1)
        email.send_keys('Ipsum')
        # time.sleep(1)
        password1.send_keys('Sit')
        # time.sleep(1)
        password2.send_keys('Sit')
        # time.sleep(1)
        submit = selenium.find_element_by_id('buttonID')
        # time.sleep(1)
        submit.click()
        # time.sleep(1)
        selenium.get(self.live_server_url + '/story9/login')

        self.assertIn("I don't have an account", selenium.page_source)

        username = selenium.find_element_by_id("username")
        password = selenium.find_element_by_id('password')

        username.send_keys('Lorem')
        password.send_keys('Sit')
        submit = selenium.find_element_by_id('buttonID')
        # time.sleep(1)
        submit.click()

        self.assertNotIn("STRANGER", selenium.page_source)